﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Index.aspx.cs" Inherits="Index" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Login | CRM </title>

    <link href="Login/css/bootstrap.css" rel="stylesheet" />
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->

    <script src="Login/js/jquery.min.js"></script>
    <!-- Custom Theme files -->

    <link href="Login/css/style.css" rel="stylesheet" />
    <!-- Custom Theme files -->
    <!---- start-smoth-scrolling---->
    <script src="Login/js/move-top.js"></script>
    <script src="Login/js/easing.js"></script>

    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            $(".scroll").click(function (event) {
                event.preventDefault();
                $('html,body').animate({ scrollTop: $(this.hash).offset().top }, 1000);
            });
        });
		</script>
    <!---- start-smoth-scrolling---->

    <script type="application/x-javascript"> 
         addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>

    <!-- webfonts -->
    <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css' />
    <link href='http://fonts.googleapis.com/css?family=Asap:400,700' rel='stylesheet' type='text/css' />
    <!-- webfonts -->

</head>
<body id="page-top">
    <form id="form1" runat="server">
        <div id="totop" class="header">
            <!-- header-info -->
            <div class="header-info text-center" style="top: 50px">
                <h2>We've got the special power</h2>
                <span></span>
                <h1>We make CRM</h1>
                <div class="col-md-12">
                     <div class="col-md-1"></div>
                    <div class="col-md-4"><a class="more" href="ProbusLogin.aspx">Probus Login</a><br />
                    </div>
                    <div class="col-md-2"></div>
                    <div class="col-md-4"><a class="more" href="UserLogin.aspx">Client Login</a><br />
                    </div>

                  
                </div>
            </div>
        </div>
    </form>
</body>
</html>
