﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="index2.aspx.cs" Inherits="index2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <title>CRM ::Login</title>
    <!-- BOOTSTRAP CORE STYLE CSS -->

    <link href="DemoLogin/css/bootstrap.css" rel="stylesheet" />
    <!-- FONT AWESOME CSS -->

    <link href="DemoLogin/css/font-awesome.min.css" rel="stylesheet" />
    <!-- CUSTOM STYLE CSS -->

    <link href="DemoLogin/css/style.css" rel="stylesheet" />
    <!-- Google	Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Nova+Flat' rel='stylesheet' type='text/css' />
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,300' rel='stylesheet' type='text/css' />
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>

          <!--  Jquery Core Script -->
    <script src="DemoLogin/js/jquery-1.10.2.js"></script>
    <!--  Core Bootstrap Script -->
    <script src="DemoLogin/js/bootstrap.js"></script>

</head>
<body style="background-image:url(Login/images/header-bg.jpg)">
    <form id="form1" runat="server">

        <section style="padding: 100px 0px 0px 0px;">
            <div class="container">
                <div class="row">

                    <div class="col-lg-6 col-lg-offset-3  col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3 ">
                        <div class="alert alert-info">
                            <div class="media">
                                <div class="pull-left">

                                    <img src="DemoLogin/img/admin.png" class="img-responsive" />
                                </div>
                                <div class="media-body">
                                    <h3 class="media-heading">Admin Login Instructions</h3>
                                    <p>
                                        Aenean faucibus luctus enim. Duis quis sem risu suspend lacinia elementum nunc. 
                                Aenean faucibus luctus enim. 
                                    </p>
                                    <a href="AdminDashboard.aspx" class="btn btn-primary" target="_blank">Go TO Amin Login</a>
                                </div>
                            </div>


                        </div>
                        <div class="alert alert-danger">
                            <div class="media">
                                <div class="pull-left">

                                    <img src="DemoLogin/img/admin.png" class="img-responsive" />
                                </div>
                                <div class="media-body">
                                    <h3 class="media-heading">User Login Instructions</h3>
                                    <p>
                                        Aenean faucibus luctus enim. Duis quis sem risu suspend lacinia elementum nunc. 
                                Aenean faucibus luctus enim. 
                                    </p>
                                    <a href="AdminDashboard.aspx" class="btn btn-danger ">Go TO User Login</a>
                                </div>
                            </div>


                        </div>
                    </div>

                </div>

            </div>

        </section>



    </form>
</body>
</html>
