﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.master" AutoEventWireup="true" CodeFile="CreateContact.aspx.cs" Inherits="CreateContact" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="main-content">
        <div class="main-content-inner">
            <div class="breadcrumbs ace-save-state" id="breadcrumbs">
                <ul class="breadcrumb">
                    <li>
                        <i class="ace-icon fa fa-home home-icon"></i>
                        <a href="#">Home</a>
                    </li>

                    <li>
                        <a href="#">Contact</a>
                    </li>
                    <li class="active">Create Contact</li>
                </ul>
                <!-- /.breadcrumb -->

                <div class="nav-search" id="nav-search">
                    <asp:Button ID="btncreate" runat="server" Text="View" CssClass="btn-success" OnClick="btncreate_Click"/>
                    <%-- <span class="input-icon">
                            <input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
                            <i class="ace-icon fa fa-search nav-search-icon"></i>
                        </span>--%>
                </div>
                <!-- /.nav-search -->
            </div>

            <div class="page-header">
							<h1>
								
								<small>
									<%--<i class="ace-icon fa fa-angle-double-right"></i>--%>
									
								</small>
							</h1>
						</div>
            <div class="row">
               <div class=" col-md-12">

                <div class=" col-md-5">
                    <div class="form-group">
                        <label for="usr">
                            Type:</label>

                        <asp:DropDownList CssClass="form-control" ID="DropDownList1" runat="server">
                            <asp:ListItem>None</asp:ListItem>
                            <asp:ListItem>Prospect</asp:ListItem>
                            <asp:ListItem>Lead</asp:ListItem>
                            <asp:ListItem>Account</asp:ListItem>

                        </asp:DropDownList>
                    </div>
                    <div class="form-group">
                        <label><b>Full Name</b></label>
                        <asp:TextBox ID="TextBox1" Class="form-control" runat="server"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label for="usr">
                            Email:</label>
                        <asp:TextBox ID="TextBox3" Class="form-control" runat="server"></asp:TextBox>

                    </div>
                    <div class="form-group">
                        <label for="usr">
                            Phone:</label>
                        <asp:TextBox ID="TextBox2" Class="form-control" runat="server"></asp:TextBox>

                    </div>
                    <div class="form-group">
                        <label for="usr">
                            Other Phone:</label>
                        <asp:TextBox ID="TextBox4" Class="form-control" runat="server"></asp:TextBox>

                    </div>
                    <div class="form-group">
                        <label for="usr">
                            Department:</label>
                        <asp:TextBox ID="TextBox8" Class="form-control" runat="server"></asp:TextBox>

                    </div>
                    <div class="form-group">
                        <label for="usr">
                            Report To:</label>

                        <asp:DropDownList CssClass="form-control" ID="DropDownList2" runat="server">
                            <asp:ListItem>None</asp:ListItem>
                        </asp:DropDownList>

                        <div class="input-group-btn  go inline" style="top: 0px">
                        </div>

                    </div>

                </div>

                <div class=" col-md-1"></div>

                <div class=" col-md-5">

                    <div class="form-group">
                        <label for="usr">
                            Parent Company:</label>
                        <asp:TextBox ID="Userid" Class="form-control" runat="server"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label for="usr">
                            Status:</label>
                        <asp:DropDownList CssClass="form-control" ID="DropDownList3" runat="server">
                            <asp:ListItem>None</asp:ListItem>

                        </asp:DropDownList>

                    </div>
                    <div class="form-group">
                        <label for="usr">
                            Priority:</label>
                        <asp:DropDownList CssClass="form-control" ID="DropDownList55" runat="server">
                            <asp:ListItem>None</asp:ListItem>

                        </asp:DropDownList>

                    </div>
                    <div class="form-group">
                        <label for="usr">
                            Transaction Type:</label>
                        <asp:DropDownList CssClass="form-control" ID="DropDownList5" runat="server">
                            <asp:ListItem>None</asp:ListItem>

                        </asp:DropDownList>

                    </div>
                    <div class="form-group">
                        <label for="usr">
                            Title:</label>

                        <asp:TextBox ID="TextBox19" Class="form-control" runat="server"></asp:TextBox>

                    </div>
                </div>


</div>
            </div>

            <div id="content" style="min-height: 600px">
                <ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
                    <li class="active"><a href="#tab1" data-toggle="tab">Address Info</a></li>
                    <li><a href="#tab2" data-toggle="tab">Payment Info</a></li>
                    <li><a href="#tab3" data-toggle="tab">Account Info</a></li>
                    <li><a href="#tab4" data-toggle="tab">Tax Document List</a></li>
                    <li><a href="#tab5" data-toggle="tab">Manage Files</a></li>
                    <li><a href="#tab3" data-toggle="tab">Additional Info</a></li>
                </ul>
                <div id="my-tab-content" class="tab-content">
                    <div class="tab-pane active " id="tab1">
                        <div class="row header" style="margin-top: 20px">
                            <div class="col-md-1">
                            </div>
                            <div class="col-md-4">
                                <asp:CheckBox ID="chkbilling" runat="server" Text="Billing Address" />
                                <asp:CheckBox ID="CheckBox1" runat="server" Text="Shipping Address" />
                                <div class="form-group">
                                    <label for="usr">
                                        Address</label>
                                    <asp:TextBox ID="Jdate" Class="form-control" runat="server"></asp:TextBox>

                                </div>
                                <div class="form-group">
                                    <label for="usr">
                                        Street</label>
                                    <asp:TextBox ID="bdate" Class="form-control" runat="server"></asp:TextBox>

                                </div>
                                <div class="form-group">
                                    <label for="usr">
                                        Area</label>
                                    <asp:TextBox ID="TextBox9" Class="form-control" runat="server"></asp:TextBox>

                                </div>
                                <div class="form-group">
                                    <label for="usr">
                                        City</label>
                                    <asp:TextBox ID="TextBox10" Class="form-control" runat="server"></asp:TextBox>

                                </div>
                                <div class="form-group">
                                    <label for="usr">
                                        Pincode</label>
                                    <asp:TextBox ID="TextBox11" Class="form-control" runat="server"></asp:TextBox>

                                </div>

                                <div class="form-group">
                                    <label for="usr">
                                        State</label>
                                    <asp:DropDownList CssClass="form-control" ID="genderDropDownList" runat="server">
                                        <asp:ListItem>Maharashtra</asp:ListItem>

                                    </asp:DropDownList>

                                </div>
                            </div>
                            <div class="col-md-4">
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane" id="tab2">
                        <div class="row header" style="margin-top: 20px">
                            <div class="row">
                                <div class="col-xs-1">
                                </div>
                                <div class="col-xs-4">
                                    <div class="form-group">
                                        <label for="usr">
                                            Bank Name:
                                        </label>
                                        <asp:TextBox ID="TextBox7" Class="form-control" runat="server"></asp:TextBox>

                                    </div>
                                    <div class="form-group">
                                        <label for="usr">
                                            Account Name:
                                        </label>
                                        <asp:TextBox ID="TextBox44" Class="form-control" runat="server"></asp:TextBox>

                                    </div>
                                    <div class="form-group">
                                        <label for="usr">
                                            Account No:
                                        </label>
                                        <asp:TextBox ID="TextBox5" Class="form-control" runat="server"></asp:TextBox>

                                    </div>
                                    <div class="form-group">
                                        <label for="usr">
                                            IFSC Code:
                                        </label>
                                        <asp:TextBox ID="TextBox6" Class="form-control" runat="server"></asp:TextBox>

                                    </div>
                                    <div class="form-group">
                                        <label for="usr">
                                            Address:
                                        </label>
                                        <asp:TextBox ID="TextBox12" Class="form-control" runat="server"></asp:TextBox>

                                    </div>
                                </div>
                                <div class="col-xs-4">
                                    <div class="form-group">
                                        <label for="usr">
                                            Payment Term:
                                        </label>
                                        <asp:TextBox ID="TextBox88" Class="form-control" runat="server"></asp:TextBox>

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane" id="tab3">
                        <div class="row header" style="margin-top: 20px">
                            <div class="col-md-1">
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="usr">
                                        Vendor Code</label>
                                    <asp:TextBox ID="TextBox192" Class="form-control" runat="server" MaxLength="6"></asp:TextBox>
                                </div>


                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="usr">
                                        Account Name
                                    </label>
                                    <asp:TextBox ID="TextBox20" Class="form-control" runat="server" MaxLength="6"></asp:TextBox>
                                </div>
                                <div class="form-group">
                                    <label for="usr">
                                        Retention Received A/C</label>
                                    <asp:TextBox ID="TextBox21" Class="form-control" runat="server" MaxLength="6"></asp:TextBox>
                                </div>
                                <div class="form-group">
                                    <label for="usr">
                                        TDS Received A/C</label>
                                    <asp:TextBox ID="TextBox13" Class="form-control" runat="server" MaxLength="6"></asp:TextBox>
                                </div>
                                <div class="form-group">
                                    <label for="usr">
                                        WCT Received A/C</label>
                                    <asp:TextBox ID="TextBox14" Class="form-control" runat="server" MaxLength="6"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane" id="tab4">
                        <div class="row header" style="margin-top: 20px">
                            <div class="col-md-1">
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="usr">
                                        Type Of Services</label>
                                    <asp:TextBox ID="TextBox15" Class="form-control" runat="server" MaxLength="6"></asp:TextBox>
                                </div>
                                <div class="form-group">
                                    <label for="usr">
                                        VAT No
                                    </label>
                                    <asp:TextBox ID="TextBox30" Class="form-control" runat="server" MaxLength="6"></asp:TextBox>
                                </div>
                                <div class="form-group">
                                    <label for="usr">
                                        CST No
                                    </label>
                                    <asp:TextBox ID="TextBox31" Class="form-control" runat="server" MaxLength="6"></asp:TextBox>
                                </div>
                                <div class="form-group">
                                    <label for="usr">
                                        CST No
                                    </label>
                                    <asp:TextBox ID="TextBox32" Class="form-control" runat="server" MaxLength="6"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="usr">
                                        Commissioner Rate Name
                                    </label>
                                    <asp:TextBox ID="TextBox16" Class="form-control" runat="server" MaxLength="6"></asp:TextBox>
                                </div>
                                <div class="form-group">
                                    <label for="usr">
                                        Pan No</label>
                                    <asp:TextBox ID="TextBox17" Class="form-control" runat="server" MaxLength="6"></asp:TextBox>
                                </div>
                                <div class="form-group">
                                    <label for="usr">
                                        Servise Tax No</label>
                                    <asp:TextBox ID="TextBox18" Class="form-control" runat="server" MaxLength="6"></asp:TextBox>
                                </div>
                                <div class="form-group">
                                    <label for="usr">
                                        RANGE No</label>
                                    <asp:TextBox ID="TextBox22" Class="form-control" runat="server" MaxLength="6"></asp:TextBox>
                                </div>
                                <div class="form-group">
                                    <label for="usr">
                                        GST No</label>
                                    <asp:TextBox ID="TextBox23" Class="form-control" runat="server" MaxLength="6"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane" id="tab5">
                        <div class="row header" style="margin-top: 20px">
                            <div class="col-md-1">
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="usr">
                                        Topics</label>
                                    <asp:TextBox ID="TextBox24" Class="form-control" runat="server" MaxLength="6"></asp:TextBox>
                                </div>
                                <div class="form-group">
                                    <label for="usr">
                                        File Description
                                    </label>
                                    <asp:TextBox ID="TextBox33" Class="form-control" runat="server" MaxLength="6"></asp:TextBox>
                                </div>
                                <div class="form-group">
                                    <label for="usr">
                                        File</label>
                                    <asp:TextBox ID="TextBox34" Class="form-control" runat="server" MaxLength="6"></asp:TextBox>
                                </div>
                                <asp:Button ID="open" class="btn btn-success" runat="server" Text="Open" Width="100px" />
                                <asp:Button ID="addnew" class="btn btn-warning" runat="server" Text="Add New" Width="100px" />
                            </div>
                            <div class="col-md-4">
                            </div>
                        </div>
                    </div>

                </div>
            </div>


        </div>
    </div>

</asp:Content>

